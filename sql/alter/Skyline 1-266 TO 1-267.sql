# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.266');

# ---------------------------------------------------------------------- #
# Add Table authorisation_types                                          #
# ---------------------------------------------------------------------- # 
CREATE TABLE authorisation_types (
									AuthorisationTypeID INT(11) NOT NULL AUTO_INCREMENT, 
									AuthorisationTypeName VARCHAR(100) NOT NULL, 
									ModifiedUserID INT(11) NULL DEFAULT NULL, 
									ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
									PRIMARY KEY (AuthorisationTypeID), 
									CONSTRAINT user_TO_authorisation_types FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) 
								  ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Modify ra_status                                                       #
# ---------------------------------------------------------------------- # 
ALTER TABLE ra_status ADD COLUMN AuthorisationTypeID INT(11) NULL DEFAULT NULL AFTER RAStatusID, 
ADD CONSTRAINT authorisation_types_TO_ra_status FOREIGN KEY (AuthorisationTypeID) REFERENCES authorisation_types (AuthorisationTypeID);


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.267');
