# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.142');


# ---------------------------------------------------------------------- #
# Modify table "manufacturer"                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer` ADD `ManufacturerLogo` VARCHAR(40) NULL AFTER `AuthReqChangedDate`;


# ---------------------------------------------------------------------- #
# Create table "manufacturer"                                            #
# ---------------------------------------------------------------------- #

CREATE TABLE `preferential_clients_manufacturers` (
	`UserID` INT(11) NOT NULL,
	`PreferentialID` INT(11) NOT NULL,
	`PageName` VARCHAR(40) NOT NULL,
	`PreferentialType` ENUM('Manufacturer','Brand') NOT NULL,
	`Priority` INT(11) NULL DEFAULT NULL,
	UNIQUE INDEX `UserID_2` (`UserID`, `PreferentialID`, `PageName`, `PreferentialType`),
	INDEX `UserID` (`UserID`, `PageName`, `PreferentialType`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.143');