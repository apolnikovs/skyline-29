{if isset($daySelected)}
{$totEngST=$ts_engineers|@count}
{$counter=0}
{$engStartTime="31500"}
{$engEndTime="67500"}
{$counter2=-1}

{if $timeSlidersStart|date_format:"%H"*1>=$timeSlidersEnd|date_format:"%H"*1}
    <div>Default engineers start and finish times are not set or are set wrong. Please go to Generel Defaults->Time Defaults and set correct values</div>
    {else}
<style>
.ui-widget-header{
    border:none !important;
    background:none !important;
    color:#444 !important;
    font-weight:bold !important;
}
.ui-slider-handle{
    position:absolute;
    z-index:200  !important;;
    width:1.2em  !important;;
    height:1.2em  !important;;
    cursor:default  !important;
}
.ui-widget-content{
    border:none !important;
}
</style>
    <div id="timeSliders_tableDiv" style="display:none;" >
        <div id="timeSlidersHolder" style="position:relative">
            <table>
                <tr>
                    <td style="width:120px">
                                                <table class="browse">
                                                    <thead>
                                                    <th style="text-align: left">Engineer</th>
                                                    </thead>
                                                    <tbody>  
                        {for $u=0 to $totEngST-1}
                <tr >
                    <td id="ts_engid_{$ts_engineers[$u].ServiceProviderEngineerID}" nowrap=nowrap style="text-align: left;background:none;height:20px;font-size: 9px;padding-left:2px;max-height:35px;">
                        <span style="border-bottom:2px solid #{$ts_engineers[$u].RouteColour}" title="{if isset($appt{$ts_engineers[$u].ServiceProviderEngineerID}) && $appt{$ts_engineers[$u].ServiceProviderEngineerID} eq 1} This Engineer is Manually Specified {/if}"> {$ts_engineers[$u].engName} {if $ts_engineers[$u].hasHoliday!=""} <span style="color:red" title="{$ts_engineers[$u].hasHoliday}">(A)</span>{/if}</span>
                        {$engPos[$ts_engineers[$u].ServiceProviderEngineerID]=$u}
                    {$engColor[$ts_engineers[$u].ServiceProviderEngineerID]=$ts_engineers[$u].RouteColour}
                    {if in_array($daySelected|date_format:"%Y-%m-%d",$finalizedDays)}
                        <input  type="checkbox" style="position:absolute;float: right;left:108px" name="engsel[]" value="{$ts_engineers[$u].ServiceProviderEngineerID}">
                        {/if}
                    </td>
                </tr>
                {/for}
                                                    </tbody>
                </table>
                    </td>
                    <td>
        <table class="browse" id="timeSlidersHeaders" style="table-layout: fixed;">
            <thead>
                 {for $e=$timeSlidersStart|date_format:"%H"*1 to $timeSlidersEnd|date_format:"%H"*1-1}
                 {$counter=$counter+1}
            <th style="text-align: left">{$e}:00</th>
                 {/for}
            </thead>
            <tbody>
                {for $u=1 to $totEngST}
                <tr id="ts_row{$u}" style="max-height:20px">
            {for $e=$timeSlidersStart|date_format:"%H"*1 to $timeSlidersEnd|date_format:"%H"*1-1}
            <td id="ts_td{$u}_{$e}" style="text-align: center;background:none;height:20px  !important;width:{100/$counter}%" ></td>
                 {/for}
                 </tr>
                 {/for}
        </tbody>
        </table>
              </td>
                </tr>
            </table>    
              {for $u=1 to $totEngST}
              <div id="ts_div{$u}" style="text-align: center;background:gray;height:5px;position:absolute"  ><div style="position:relative;border:1px dashed white;margin-top:1px"></div></div>
              <div id="ts_div_ab{$u}" style="text-align: center;background:none;height:5px;position:absolute"  ></div>
                {/for}
</div>
                 </div>
                  <div id="timeTooltip" style="position:absolute;display:none;background:white;z-index:300"><span  id="amountB"></span> - 
                  <span id="amountE"></span></div>
                <fieldset>
                    <legend>Optimisation Sliders</legend>
                    Unreachable: <span>{$unrApp|@count}</span>
                    <div id="unrContainer" style="width:700px">
                    </div>
                     <img style="float:right"  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="TimeSlidersHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
                  {if $ServiceProviderID!=64}
                     <button class="btnStandard" style="float:right" type="button" onclick="reoptimize()">Reoptimise</button>
                    {else}
                     <button class="btnStandard" style="float:right" type="button" onclick="showMsgVisual()">Reoptimise</button>
                     {/if}
                    </fieldset>
<script>
function showEngineers(s,sp,a,d)
{        
    $.post("{$_subdomain}/Diary/getEngineersToSkillset",{ s:s,sp:sp,d:d,a:a },
    function(data) {
        en=jQuery.parseJSON(data);
        $.each(en, function(key, value) {
            $("#ts_engid_"+value.ServiceProviderEngineerID).css({ 'background':'#90DC18','text_decoration':'underline'});
        });
    });
}
function showEngineersHide(s,sp,a,d){                  
    $.post("{$_subdomain}/Diary/getEngineersToSkillset",{ s:s,sp:sp,d:d,a:a },
    function(data) {
        en=jQuery.parseJSON(data);
        $.each(en, function(key, value) {
            $("#ts_engid_"+value.ServiceProviderEngineerID).css({ 'background':'white','text_decoration':'none'});
        });
    });
}
function showBaloon()
{
    $('selectors').balloon({ position: "bottom right" });
}
$(document).ready(function() {
    var hoursPx=(($('#timeSlidersHeaders').width()-2) / {$counter}*1 /3600);
    var totalWorkHours={$counter}                
    {for $u=1 to $totEngST}
        var   p=$('#ts_row{$u}').position();
        $('#ts_div{$u}').css({ 'top':p.top+16,'left':p.left+{($ts_engineers[$u-1].startTime-$timeSlidersStartSec)}*hoursPx,'width':{($ts_engineers[$u-1].endTime-$ts_engineers[$u-1].startTime)}*hoursPx})
        $('#ts_div_ab{$u}').css({ 'top':p.top+16,'left':p.left,'width':$('#ts_row{$u}').width()});
        $(function() {
            $( "#ts_div_ab{$u}" ).slider({
                range: true,
                minRange: 0,
                step:300,
                min: {$timeSlidersStartSec},
                max: {$timeSlidersEndSec},
                values: [ ({$ts_engineers[$u-1].startTime}), ({$ts_engineers[$u-1].endTime})],
                slide: function( event, ui ) {
                    secToTime(ui.values[0],ui.values[1]) ;
                    $('#ts_div{$u}').css({ 'left':(ui.values[0]-{$timeSlidersStartSec})*hoursPx+135,'width':(ui.values[1]-ui.values[0])*hoursPx});
                },
                stop:function( event, ui ){
                    $( "#timeTooltip" ).css({ 'top':mouseY-40,'left':mouseX}).fadeOut('slow');
                    $.post("{$_subdomain}/Diary/updateTimeSliders",{ s:ui.values[0],e:ui.values[1],id:{$ts_engineers[$u-1].ServiceProviderEngineerID}},
                    function(data) { });
                }
            }).css('left',($( "#ts_div_ab{$u}" ).slider().css('left')-$( "#ts_div_ab{$u}" ).slider().css('left')*0.06));
        });       
        /*
         * @author Dileep Bhimineni 
         *Create a new div from start time to end time for engineers who are absent on time slider.
        */
        var divp=$('#ts_div{$u}').position();
        {assign var=incr value=0}
        {foreach from=$ts_engineers[$u-1] key=k item=v}
            {if is_array($v)}
                {foreach from=$v item=engTimeData}
                    {if $engTimeData.hasHoliday != ""}
                        {if $engTimeData.allDayHoliday eq 'yes'}
                            var left = p.left+Math.round(2700*hoursPx);
                            var width = 29700*hoursPx ;
                            var title = "Engineer is Absent all Day" ;
                            var alt = "Engineer is Absent all Day" ;
                        {else}
                            var left = p.left+Math.round({($engTimeData.holidayStart-$timeSlidersStartSec)}*hoursPx);
                            var width = {$engTimeData.holidayEnd-$engTimeData.holidayStart}*hoursPx ;
                            if(width > 265)
                               var title = "Engineer is Absent between {$engTimeData.holidayStartTime} - {$engTimeData.holidayEndTime}";
                            else
                                var title = "Absent";
                            var alt = "Engineer is Absent between {$engTimeData.holidayStartTime} - {$engTimeData.holidayEndTime}";
                        {/if}
                        $('#timeSlidersHolder').append('<div id="holiday_{$u}_{$incr}" title="' + alt + '" style=";position:absolute;border:2px solid #cccccc;z-index:100;background:white;text-align:center">'+ title +'</div>');
                        $('#holiday_{$u}_{$incr}').css({ 'left':left,'width':width,'top':divp.top-7});
                    {/if}
                    {$incr++}
                {/foreach}
            {/if}
        {/foreach}
    {/for}
    {foreach from=$ts_apps key=k item=v}
        {$counter2=$counter2+1}
        {while $engPos[$k]!=$counter2}{$counter2=$counter2+1} {/while}
        {$counter3=1}
        {$startMarker=0}
        {foreach from=$v item=i}
            {if $startMarker==0}
                var pp=$('#ts_div1').position();
                $('#timeSlidersHolder').append('<div id="{$k}_start_{$i.EngstartTimeSec}" title="Viamente Start Time: {$i.EngstartTime|date_format:"%H:%M"}"  style="position:absolute;border-left:4px solid  red;z-index:100;text-align:center"></div>')      
                $('#{$k}_start_{$i.EngstartTimeSec}').css({ 'left':p.left+Math.round({($i.EngstartTimeSec-$timeSlidersStartSec)}*hoursPx),'width':1,'top':pp.top+{($counter2)*35}-8,'height':'20'});
                {$startMarker=1}
            {/if}
            var pp=$('#ts_div1').position();
            $('#timeSlidersHolder').append('<div id="{$k}_idle_{$i.idle}" title="Idle Time "  style=";position:absolute;border:2px solid  black;z-index:100;background:black;text-align:center"></div>')      
            $('#{$k}_idle_{$i.idle}').css({ 'left':p.left+Math.round({($i.idle-$timeSlidersStartSec)}*hoursPx),'width':{$i.ap_st-$i.idle}*hoursPx,'top':pp.top+{($counter2)*35}-4,'height':'10'});
            $('#timeSlidersHolder').append('<div id="{$k}_{$i.ap_st}" title="{$i.postcode} {$i.SkillsetName} "  style=";position:absolute;border:2px solid  #{$engColor[$k]};z-index:100;background:{if $i.slotCol!=""}#{$i.slotCol}{else}white{/if};text-align:center">{$counter3}</div>')      
            $('#{$k}_{$i.ap_st}').css({ 'left':p.left+Math.round({($i.ap_st-$timeSlidersStartSec)}*hoursPx),'width':{$i.ap_en-$i.ap_st}*hoursPx,'top':pp.top+{($counter2)*35}-8});
            {if $counter3==$v|@count}
                var pp=$('#ts_div1').position();
                $('#timeSlidersHolder').append('<div id="{$k}_end_{$i.EngEndTimeSec}" title="Viamente Finish Time: {$i.EngEndTime|date_format:"%H:%M"}"  style="position:absolute;border-left:4px solid  red;z-index:100;text-align:center"></div>')      
                $('#{$k}_end_{$i.EngEndTimeSec}').css({ 'left':p.left+Math.round({($i.EngEndTimeSec-$timeSlidersStartSec)}*hoursPx),'width':1,'top':pp.top+{($counter2)*35}-8,'height':'20'}); 
            {/if}
            {$counter3=$counter3+1}
        {/foreach}
    {/foreach}
    {for $u2=0 to $unrApp|@count-1}
        $('#unrContainer').append('<div onmouseOut=showEngineersHide({$unrApp[$u2].ServiceProviderSkillsetID},{$unrApp[$u2].ServiceProviderID},"{$unrApp[$u2].AppointmentDate}", {$unrApp[$u2].AppointmentID}); onmouseOver=showEngineers({$unrApp[$u2].ServiceProviderSkillsetID},{$unrApp[$u2].ServiceProviderID},"{$unrApp[$u2].AppointmentDate}", {$unrApp[$u2].AppointmentID}) id="unr_{$unrApp[$u2].AppointmentID}" title="{$unrApp[$u2].postcode} {$unrApp[$u2].SkillsetName} {if isset($unrApp[$u2].engName)} \n specified to {$unrApp[$u2].engName} {/if}"  style="float:left;position:relative;border:2px solid  gray;z-index:100;background:gray;text-align:center;color:white;margin:5px;font-size:10px">{$unrApp[$u2].Duration}</div>')     
        $('#unr_{$unrApp[$u2].AppointmentID}').css({ 'width':({$unrApp[$u2].Duration*60})*hoursPx});
    {/for}
    $('.ui-widget-header').css("border",'none !important');
});
function  secToTime(sec,sec2) {
    sec_numb    = parseInt(sec);
    var hours   = Math.floor(sec_numb / 3600);
    var minutes = Math.floor((sec_numb - (hours * 3600)) / 60);
    var seconds = sec_numb - (hours * 3600) - (minutes * 60);
    
    if (hours   < 10) { hours   = "0"+hours;}
    if (minutes < 10) { minutes = "0"+minutes;}
    if (seconds < 10) { seconds = "0"+seconds;}
    var time    = hours+':'+minutes;
    sec_numb2    = parseInt(sec2);
    var hours   = Math.floor(sec_numb2 / 3600);
    var minutes = Math.floor((sec_numb2 - (hours * 3600)) / 60);
    var seconds = sec_numb2 - (hours * 3600) - (minutes * 60);
    
    if (hours   < 10) { hours   = "0"+hours;}
    if (minutes < 10) { minutes = "0"+minutes;}
    if (seconds < 10) { seconds = "0"+seconds;}
    var time2    = hours+':'+minutes;
    $( "#amountB" ).html(time);
    $( "#amountE" ).html(time2);
    $( "#timeTooltip" ).css({ 'top':mouseY-40,'left':mouseX}).fadeIn('slow');
}
function reoptimize()
{
    {if in_array($daySelected|date_format:"%Y-%m-%d",$finalizedDays)}
        var values = new Array();
        $.each($("input[name='engsel[]']:checked"), function() {
          values.push($(this).val());
        });
        if(values.length!=0){
            insertWaitL(6);
            $.post("{$_subdomain}/Diary/optimiseIndEngineers",{ v:values },
            function(data) {
                window.location="{$_subdomain}/Diary/default/timeSliders=1/emails=1";
            });
        }
        else
            alert("Day is finalized please select engineers you need reoptimise first!")
    {else}
        insertWaitL(6);
        window.location="{$_subdomain}/Diary/reoptimize/oneEng=1";
    {/if}
}
$(document).ready(function() {
    {if isset($args.emails)&&$args.emails==1}var plop=1;{else}var plop=0;{/if}
    if(plop==1){
        $.colorbox({
            html:"<div> <h1 style='color:#8FD380'>Reoptimisation completed.</h1><input type='checkbox' onclick='sendemailsind();'> Send email(s) to engineers reoptimised <p class='endMsgP1'></p></div>",
            title: "Reoptimisation complete",
            opacity: 0.75,
            width:500,
            overlayClose: false,
            escKey: false
        });
    }
});
function sendemailsind(){
    $('.endMsgP1').html("<span style='color:red'>Please wait. Sending emails.</span>");
    $.post("{$_subdomain}/Diary/emailViamnteMap",{ 'mode':2 },
    function(data) {
        $('.endMsgP1').html("<span style='color:green'>Emails sent, you can close this window.</span>");
    });
}
function showMsgVisual(){
    $.colorbox({
        html:$('#ViaDiv1Timesliders').html(),
        title: "Reoptimisation complete",
        opacity: 0.75,
        overlayClose: false,
        escKey: false
    }); 
}
</script>
{/if}         
{/if} 
                  
                  
                  <div id="ViaDiv1Timesliders" style="display:none;font-size: 14px;width:500px;margin-bottom: 0px;height:150px">
     <div style="font-size: 14px;width:500px;margin-bottom: 0px;">
         <fieldset>
             <legend>Routine Optimisation</legend>
       


    
        <a > <button id=""  type=button class="btnStandard"   onclick="insertWaitL(7)" style="width:200px;float:left"><span class="label">Routine Optimisation</span></button>   </a>
       
        <div style="margin-top:30px"><br>
            
        <input onclick="$('#vtype').val('Brown')" type="radio"  name="vtype" value="Brown">Brown Goods
        <input onclick="$('#vtype').val('White')" type="radio" name="vtype" value="White">White Goods
        <input onclick="$('#vtype').val('Both')" checked="checked" type="radio" name="vtype" value="Both">Both
        
        </div>
     
         </fieldset>
       </div> </div>  