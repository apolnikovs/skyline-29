<style>
    .SystemAdminFormPanel label.fieldError {
        margin-left: 180px;
        }
 </style>
<script type="text/javascript">
    $(document).ready(function() {
    
     $("#SQbId").combobox();


    
$(function() {
	$( ".datepicker" ).datepicker({
		dateFormat: 'dd/mm/yy',
		changeMonth: true,
		changeYear: false,
                minDate: "+1d",
               defaultDate: "+1d"
	});
});
    });
</script>
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <div id="satisfactionQuestionnaireFormPanel" class="SystemAdminFormPanel" >
    
                <form id="SQForm" name="SQForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
           
                       
                <p>
                            <label class="fieldLabel" for="Name" >{$page['Labels']['brand_name']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp;  <select name="SQbId" id="SQbId" >
                            <option value="" {if $bId eq ''}selected="selected"{/if}> Select Brand </option>                             

                            {foreach $brands as $brand}
                                <option value="{$brand.BrandID}" {if $bId eq $brand.BrandID}selected="selected"{/if}>{$brand.BrandName|escape:'html'}</option>

                            {/foreach}               
                            </select>
                        
                </p>
                {if isset($datarow.Who_suggested_our_services_to_you)}{assign var=radio value="|"|explode:$datarow.Who_suggested_our_services_to_you}{/if}          
               <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_1']|escape:'html'}" >{$page['Labels']['radio_buttoin_1']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[0]}" name="attr_10[]" class="text" />
                         </p>       

                
                <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_2']|escape:'html'}" >{$page['Labels']['radio_buttoin_2']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[1]}" name="attr_10[]" class="text" />
                         </p>                  
                         

                 <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_3']|escape:'html'}" >{$page['Labels']['radio_buttoin_3']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[2]}" name="attr_10[]" class="text" />
                         </p>   
                
                 <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_4']|escape:'html'}" >{$page['Labels']['radio_buttoin_4']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[3]}" name="attr_10[]" class="text" />
                         </p>                 
                  <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_5']|escape:'html'}" >{$page['Labels']['radio_buttoin_5']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[4]}" name="attr_10[]" class="text" />
                         </p>   
                  <p>
                            <label class="fieldLabel" for="{$page['Labels']['radio_buttoin_6']|escape:'html'}" >{$page['Labels']['radio_buttoin_6']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" value="{$radio[5]}" name="attr_10[]" class="text" />
                         </p>    
                     <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_1']|escape:'html'}" >{$page['Labels']['additiona_question_1']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" class="text" value="{$datarow.ease_of_booking}" name="attr_4" />
                         </p> 
                         
                     <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_2']|escape:'html'}" >{$page['Labels']['additiona_question_2']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" class="text" value="{$datarow.speed_of_service_first_visit}" name="attr_5" />
                         </p> 
                    <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_3']|escape:'html'}" >{$page['Labels']['additiona_question_3']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" class="text" value="{$datarow.speed_of_service_overall}" name="attr_6" />
                         </p>      
                   <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_4']|escape:'html'}" >{$page['Labels']['additiona_question_4']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" class="text" value="{$datarow.communication}" name="attr_7" />
                         </p>
                      <p>
                            <label class="fieldLabel" for="{$page['Labels']['additiona_question_5']|escape:'html'}" >{$page['Labels']['additiona_question_5']|escape:'html'}: </label>
                           
                             &nbsp;&nbsp; <input type="text" class="text" value="{$datarow.engineer}" name="attr_8" />
                         </p>
                      
                         <p>
                            <label class="fieldLabel" for="{$page['Labels']['implementation_date']|escape:'html'}" >{$page['Labels']['implementation_date']|escape:'html'}: </label>
                           
                            &nbsp;&nbsp; <input type="text" class="text datepicker" value="{"tomorrow"|date_format:"%d/%m/%Y"}" name="implementationDate" readonly="readonly" />
                         </p>



                                    
                                        
                              <p>
                    
                                <span class= "bottomButtons" >            
                                    
                                     <br>
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        <br>
                                   
                                        <input type="submit" name="cancel_btn" class="textSubmitButton  rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
    
 
{* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="location.reload();"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
