<?php

require_once('CustomModel.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Town Allocations Page in Job Allocation section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class TownAllocations extends CustomModel {
    
    private $conn;
    
    private $table                      = "town_allocation";
    private $table_network              = "network";
    
   
   
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->table 
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID       = isset($args['firstArg'])?$args['firstArg']:'';
        $ManufacturerID  = isset($args['secondArg'])?$args['secondArg']:'';
        $CountryID       = isset($args['thirdArg'])?$args['thirdArg']:'';
        
        
        $dbTablesColumns  = array('T1.TownAllocationID', 'T2.Name', 'T1.Town', 'T3.RepairSkillName',  'T4.ClientName', 'T5.Type', 'T6.ServiceTypeName', 'T7.CompanyName', 'T1.Status');
        $dbTables         = $this->table." AS T1 
            
                            LEFT JOIN county AS T2 ON T1.CountyID=T2.CountyID
                            LEFT JOIN repair_skill AS T3 ON T1.RepairSkillID=T3.RepairSkillID
                            LEFT JOIN client  AS T4 ON T1.ClientID =T4.ClientID 
                            LEFT JOIN job_type  AS T5 ON T1.JobTypeID=T5.JobTypeID
                            LEFT JOIN service_type AS T6 ON T1.ServiceTypeID=T6.ServiceTypeID
                            LEFT JOIN service_provider AS T7 ON T1.ServiceProviderID=T7.ServiceProviderID
                            ";
                
        
        if($NetworkID!='')
        {
            $countryClause = '';
            if($CountryID)
            {
                $countryClause = " AND T1.CountryID='".$CountryID."'";
            }    
            
            if($ManufacturerID)
            {
                $args['where']    = "T1.NetworkID='".$NetworkID."' AND T1.ManufacturerID='".$ManufacturerID."'".$countryClause;
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }   
            else
            {    
                $args['where']    = "T1.NetworkID='".$NetworkID."'".$countryClause;
                
                $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
            }
        }
        else
        {
            $args['where']    = "T1.TownAllocationID='0'";
            
             $output = $this->ServeDataTables($this->conn, $dbTables, $dbTablesColumns, $args);
        }
       
        //$this->controller->log(var_export($output, true));
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['TownAllocationID']) || !$args['TownAllocationID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to check for duplicate row.
     *
     * @param array $args It contains list of fields
     
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($TownAllocationID, $args) {
        
        $prepArgs = array(
            
           ':NetworkID' => $args['NetworkID'], 
           ':CountryID' => $args['CountryID'], 
           ':ManufacturerID' => $args['ManufacturerID'], 
           ':RepairSkillID' => $args['RepairSkillID'], 
           ':JobTypeID' => $args['JobTypeID'], 
           ':ServiceTypeID' => $args['ServiceTypeID'], 
           ':Town' => $args['Town'], 
           ':CountyID' => $args['CountyID'], 
           ':ClientID' => $args['ClientID'], 
           ':ServiceProviderID' => $args['ServiceProviderID'], 
           ':TownAllocationID' => $TownAllocationID
            
        ); 
         
         
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT TownAllocationID FROM '.$this->table.' WHERE NetworkID=:NetworkID AND 
        
        CountryID=:CountryID AND
        ManufacturerID=:ManufacturerID AND
        RepairSkillID=:RepairSkillID AND
        JobTypeID=:JobTypeID AND
        ServiceTypeID=:ServiceTypeID AND
        Town=:Town AND
        CountyID=:CountyID AND
        ClientID=:ClientID AND
        ServiceProviderID=:ServiceProviderID AND
        TownAllocationID!=:TownAllocationID';
       
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute($prepArgs);
        $result = $fetchQuery->fetch();
                
        
        if(is_array($result) && $result['TownAllocationID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
   
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
      
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        if($this->isValid(0, $args))
        {  
                $result = false;
                    
                /* Execute a prepared statement by passing an array of values */
                $sql = 'INSERT INTO '.$this->table.' (NetworkID, CountryID, ManufacturerID, RepairSkillID, JobTypeID, ServiceTypeID, Town, CountyID, ClientID, ServiceProviderID, Status, CreatedDate, ModifiedUserID, ModifiedDate)
                VALUES(:NetworkID, :CountryID, :ManufacturerID, :RepairSkillID, :JobTypeID, :ServiceTypeID, :Town, :CountyID, :ClientID, :ServiceProviderID, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';

                $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

                $result =  $insertQuery->execute(array(

                    ':NetworkID' => $args['NetworkID'],
                    ':CountryID' => $args['CountryID'],
                    ':ManufacturerID' => $args['ManufacturerID'],
                    ':RepairSkillID' => $args['RepairSkillID'],
                    ':JobTypeID' => $args['JobTypeID'],
                    ':ServiceTypeID' => $args['ServiceTypeID'],
                    ':Town' => $args['Town'],
                    ':CountyID' => $args['CountyID'],
                    ':ClientID' => $args['ClientID'],
                    ':ServiceProviderID' => $args['ServiceProviderID'],
                    ':Status' => $args['Status'],
                    ':CreatedDate' => date("Y-m-d H:i:s"),
                    ':ModifiedUserID' => $this->controller->user->UserID,
                    ':ModifiedDate' => date("Y-m-d H:i:s")

                    ));
                

                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_inserted_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @global $this->table_network
     *  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT TownAllocationID, NetworkID, CountryID, ManufacturerID, RepairSkillID, JobTypeID, ServiceTypeID, Town, CountyID, ClientID, ServiceProviderID, Status FROM '.$this->table.' WHERE TownAllocationID=:TownAllocationID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':TownAllocationID' => $args['TownAllocationID']));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['NetworkID'])
        {
            //Getting network name.
            $sql3        = 'SELECT CompanyName FROM '.$this->table_network.' WHERE NetworkID=:NetworkID';
            $fetchQuery3 = $this->conn->prepare($sql3, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery3->execute(array(':NetworkID' => $result['NetworkID']));
            $result3     = $fetchQuery3->fetch();
            $result['NetworkName']  = isset($result3['CompanyName'])?$result3['CompanyName']:'';
            
        }
        else
        {
            $result['NetworkName'] = '';
        }
     
        
        return $result;
     }
    
     
     
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
        
     * @global $this->table 
     
     *    
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        
        if($this->isValid($args['TownAllocationID'], $args))
        {        
                $EndDate = "0000-00-00 00:00:00";
                $row_data = $this->fetchRow($args);
                if($this->controller->statuses[1]['Code']==$args['Status'])
                {
                    if($row_data['Status']!=$args['Status'])
                    {
                            $EndDate = date("Y-m-d H:i:s");
                    }
                }
            
            
                /* Execute a prepared statement by passing an array of values */
                $sql = 'UPDATE '.$this->table.' SET NetworkID=:NetworkID, CountryID=:CountryID, ManufacturerID=:ManufacturerID, RepairSkillID=:RepairSkillID, JobTypeID=:JobTypeID, ServiceTypeID=:ServiceTypeID, Town=:Town, CountyID=:CountyID, ClientID=:ClientID, ServiceProviderID=:ServiceProviderID, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate WHERE TownAllocationID=:TownAllocationID';


                $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result = $updateQuery->execute(


                        array(

                                ':NetworkID' => $args['NetworkID'],
                                ':CountryID' => $args['CountryID'],
                                ':ManufacturerID' => $args['ManufacturerID'],
                                ':RepairSkillID' => $args['RepairSkillID'],
                                ':JobTypeID' => $args['JobTypeID'],
                                ':ServiceTypeID' => $args['ServiceTypeID'],
                                ':Town' => $args['Town'],
                                ':CountyID' => $args['CountyID'],
                                ':ClientID' => $args['ClientID'],
                                ':ServiceProviderID' => $args['ServiceProviderID'],
                                ':Status' => $args['Status'],
                                ':EndDate' => $EndDate,
                                ':ModifiedUserID' => $this->controller->user->UserID,
                                ':ModifiedDate' => date("Y-m-d H:i:s"),
                                ':TownAllocationID' => $args['TownAllocationID']  

                            )

                        );
        
               
               
                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_updated_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
              
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
   
    
    
}
?>