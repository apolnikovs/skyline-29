/************************************************** 
 * 
 *  Updated for SkyLine project use
 *  
 *  - global functions moved top the top of file
 * 
 *  
 *   
 **************************************************/



function urlencode (str) {
            str = (str + '').toString();
            return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } 

        

(function( $ ){
    
    
    
    /* Get the rows which are currently selected */
    function fnGetSelected( oTableLocal ) {
        var aReturn = new Array();
        var aTrs = oTableLocal.fnGetNodes();

        for ( var i=0 ; i<aTrs.length ; i++ ) {
            if ( $(aTrs[i]).hasClass('row_selected') ) {
                aReturn.push( aTrs[i] );
            }
        }
        return aReturn;
    }



   function info() 
{
                var _info = new Array( 'SkyLine');
                return _info;
}

    

    function updateHeight($eId) {
        
        if(location.href != top.location.href){ 
            // the content has been loaded into an IFRAME
            // so tell parent to resize IFRAME
            $.postMessage(
            
                    'if_height=' + $('body').outerHeight(true),
                    options['subdomain']+'/product/',
                    parent
            );
                
             
        }
        else
            {
                if($eId!='' && $eId)
                    {
                        if($.colorbox)
                            {
                        
                                $.colorbox.resize({
                                    height: ($('#'+$eId).height()+150)+"px"
                                });
                            }
                    }    
            }
        
    }

   function prepareId($arg1, $arg2)
   {
       if($arg1=='')
           {
               $arg1 = $arg2;
           }
       return $arg1;
   }

  
 

$.fn.PCCSDataTable = function( optionsOrg ) {  

    // Create some defaults, extending them with any options that were provided

    var $orgHtmlTableId = '';
    if(optionsOrg['htmlTableId']!='' && optionsOrg['htmlTableId'])
        {
            $orgHtmlTableId = optionsOrg['htmlTableId'];
        }

var options = $.extend({
                        
                        displayButtons:'', // Write A for Add, U for Update, D for Delete, V for view and P Pick...Ex: AUD
                        htmlTableId: 'myBrowse',
                        htmlTablePageId: 'myBrowsePageId',
                        addButtonId:    $orgHtmlTableId+'addButtonId',
                        updateButtonId: $orgHtmlTableId+'updateButtonId',
                        deleteButtonId: $orgHtmlTableId+'deleteButtonId',
                        viewButtonId:   $orgHtmlTableId+'viewButtonId',
                        pickButtonId:   $orgHtmlTableId+'pickButtonId',
                        
                        formInsertButton: $orgHtmlTableId+'formInsertButton',
                        formUpdateButton: $orgHtmlTableId+'formUpdateButton',
                        formDeleteButton: $orgHtmlTableId+'formDeleteButton',
                        formCancelButton: $orgHtmlTableId+'formCancelButton',
                        formViewButton: $orgHtmlTableId+'formViewButton',
                        
                        
                        
                        fetchDataUrl:  '',
                        updateDataUrl: '',
                        createDataUrl: '',
                        deleteDataUrl: '',
                        createAppUrl:  '',
                        updateAppUrl:  '',
                        viewAppUrl:    '',
                        deleteAppUrl:  '',
                        
                        colorboxFormId: $orgHtmlTableId+'colorboxFormId',
                        parentURL: false,
                        
                        
                        popUpFormWidth: 0, //Put 0 for default width
                        popUpFormHeight: 600, //Put 0 for default height
                        
                        frmErrorRules: {},
                        frmErrorMessages: { },
                        frmErrorMsgClass: 'webformerror',
                        frmErrorElement: 'label',
                        
                        isExistsDataUrl: false,
                        suggestTextId: $orgHtmlTableId+'suggestTextId',
                        sugestFieldId: $orgHtmlTableId+'sugestFieldId',
                        frmErrorSugMsgClass: 'suggestwebformerror',
                        
                        hiddenPK: false,
                        passParamsType:'0',
                        superFilter: {},
                        subdomain: window.location,
                        searchCloseImage:'',
                       
                       
                       
                        bServerSide:true,
                        bProcessing:true,
                        bDeferRender:true,
                        sDom: 'ft<"dataTables_command">rpli',
                        addButtonText:'Add',
                        updateButtonText:'Update',
                        deleteButtonText:'Delete',
                        viewButtonText:'View',
                        pickButtonText:'Select',
                        searchBoxLabel: 'Search within results: ',
                        pickCallbackMethod: '',
                        dblclickCallbackMethod: '',
                        tooltipTitle:''


                    } , optionsOrg);
                    
                    
        var oTable;    
        var passParamsTypeStr;
        
        //Primary key parmater passing type.
        if(options['passParamsType']=='1') {
               passParamsTypeStr = "?pk="; 
        } else {
               passParamsTypeStr = ""; 
        }                 

       
        
        
         //Assing false to button ids if button id value is ''
        options['htmlTablePageId'] = prepareId(options['htmlTablePageId'], options['htmlTableId']+"PageId"); 
        options['addButtonId'] = prepareId(options['addButtonId'], options['htmlTableId']+"addButtonId");
        options['updateButtonId'] = prepareId(options['updateButtonId'], options['htmlTableId']+"updateButtonId");
        options['deleteButtonId'] = prepareId(options['deleteButtonId'], options['htmlTableId']+"deleteButtonId");
        options['viewButtonId'] = prepareId(options['viewButtonId'], options['htmlTableId']+"viewButtonId");
        options['pickButtonId'] = prepareId(options['pickButtonId'], options['htmlTableId']+"pickButtonId");
        options['formInsertButton'] = prepareId(options['formInsertButton'], options['htmlTableId']+"formInsertButton");
        options['formUpdateButton'] = prepareId(options['formUpdateButton'], options['htmlTableId']+"formUpdateButton");
        options['formDeleteButton'] = prepareId(options['formDeleteButton'], options['htmlTableId']+"formDeleteButton");    
        options['formCancelButton'] = prepareId(options['formCancelButton'], options['htmlTableId']+"formCancelButton");    
        options['formViewButton'] = prepareId(options['formViewButton'], options['htmlTableId']+"formViewButton");    
        
        options['colorboxFormId'] = prepareId(options['colorboxFormId'], options['htmlTableId']+"colorboxFormId"); 
        options['suggestTextId'] = prepareId(options['suggestTextId'], options['htmlTableId']+"suggestTextId");       
        options['sugestFieldId'] = prepareId(options['sugestFieldId'], options['htmlTableId']+"sugestFieldId");  
        options['sDom'] = prepareId(options['sDom'], 'ft<"dataTables_command">rpli');                 
               
               
             
        
        
      //If user provides fetch data url then we are assigning true values to the following options.  
      if(options['fetchDataUrl']!='' && options['fetchDataUrl'])
      {
          
            if(options['bServerSide'])
                {
                    options['bServerSide']  = true; 
                }
            
            if( options['bProcessing'])
                {
                    options['bProcessing']  = true;  
                }
                
            if(options['bDeferRender'])
                {
                    options['bDeferRender']  = true;  
                }
      }
      
     
        

    return this.each(function() {    
            
         
       $('#'+options['htmlTableId']+' tbody').mouseover( function() {
           
           this.setAttribute( 'title', options['tooltipTitle'] );
           
        } );    
            
            
            
        //Double click action on data table row - starts here..    
        $('#'+options['htmlTableId']+' tbody').dblclick(function(event) {
            
                var sltdRowData = new Array();
                var nTds = $('td', event.target.parentNode);
               
                
                for($i=0;$i<nTds.length;$i++)
                {
                    sltdRowData[$i] = $(nTds[$i]).text();
                }
                
                if(options['dblclickCallbackMethod'] && options['dblclickCallbackMethod']!='')
                {
                    eval(options['dblclickCallbackMethod']+'(sltdRowData);');
                } 
           
        } ); 
       //Double click action on data table row - ends here..     
            
            
            
        /* Add a click handler to the rows - this could be used as a callback */
        $('#'+options['htmlTableId']+' tbody').click(function(event) {
            if ($(event.target.parentNode).hasClass('row_selected')) {
                
                $(event.target.parentNode).removeClass('row_selected');
               
                    if(options['updateButtonId']!='' && options['updateButtonId'])
                        {
                            $('#'+options['updateButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }

                    if(options['deleteButtonId']!='' && options['deleteButtonId'])
                        {
                                $('#'+options['deleteButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }   

                    if(options['viewButtonId'] && options['viewButtonId']!='')
                        {
                            $('#'+options['viewButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }    
                    if(options['pickButtonId'] && options['pickButtonId']!='')
                        {
                                $('#'+ options['pickButtonId']).attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');
                        }    
            
            } else {
                
                $(oTable.fnSettings().aoData).each(function () {
                    $(this.nTr).removeClass('row_selected');
                } );
                
                $(event.target.parentNode).addClass('row_selected');
                
                if(options['updateButtonId'] && options['updateButtonId']!='')
                    {
                        $('#'+options['updateButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                    } 

                if(options['deleteButtonId']!='' && options['deleteButtonId'])
                    {
                        $('#'+options['deleteButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                    }

                if(options['viewButtonId'] && options['viewButtonId']!='')
                    {
                            $('#'+options['viewButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                    }

                if(options['pickButtonId'] && options['pickButtonId']!='')
                    {
                            $('#'+ options['pickButtonId']).removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                    }    
            
            }            
        } );
        
        
                    $(document).on('click', '#'+options['htmlTableId']+'_filter img',  function(){
            $(this).parent().find('input').val('').keyup();
        });

        /* Add a click handler to the add button of datatable */
        $(document).on('click', '#'+options['addButtonId'], 
            function() {

                

                // Ignore action if button disabled
                if (!$('#'+options['addButtonId']).attr('disabled')) {


                                if(options['createAppUrl']!='' && options['createAppUrl'])
                                {
                                    //It opens color box popup page.              
                                    $.colorbox( { href: options['createAppUrl'],
                                                    title: 'Add new record',
                                                    opacity: 0.75,
                                                    height:options['popUpFormHeight'],
                                                    width:options['popUpFormWidth'],
                                                    overlayClose: false,
                                                    escKey: false } );
                                }
            }      
                        }      
        );
            
        


                    /* Add a click handler to the update button of datatable */
                    $(document).on('click', '#'+options['updateButtonId'], 
                            function() {
                                // Ignore action if button disabled
                                if (!$('#'+options['updateButtonId']).attr('disabled')) {
                                    var anSelected = fnGetSelected( oTable );

                                    if(options['updateAppUrl'] && options['updateAppUrl']!='')
                                        {
                                            //It opens color box popup page.  
                                            $.colorbox( { href:options['updateAppUrl']+passParamsTypeStr+urlencode(oTable.fnGetData(anSelected[0],0)),
                                                        title: 'Update record',
                                                        height:options['popUpFormHeight'],
                                                        width:options['popUpFormWidth'],
                                                        opacity: 0.75,     
                                                        overlayClose: false,
                                                        escKey: false,
                                                                onComplete: function(){
                                                                    if(options['sugestFieldId'] && options['sugestFieldId']!='')
                                                                        {
                                                                            $('#'+options['sugestFieldId']).attr('disabled', 'disabled'); 
                                                                        }
                                                                }
                                                } );
                                        }
                            }      
                            }      
                        );



                        /* Add a click handler to the delete button of datatable */   
                        $(document).on('click', '#'+options['deleteButtonId'], 
                            function() {
                                
                                if(options['deleteButtonId']!='' && options['deleteButtonId'])
                                    {
                                        // Ignore action if button disabled
                                        if (!$('#'+options['deleteButtonId']).attr('disabled')) {


                                        var anSelected = fnGetSelected( oTable );

                                            if(options['deleteAppUrl'] && options['deleteAppUrl']!='')
                                                {
                                                    //It opens color box popup page. 
                                                    $.colorbox( { href:options['deleteAppUrl']+passParamsTypeStr+urlencode(oTable.fnGetData(anSelected[0],0)),
                                                                title: 'Delete record',
                                                                opacity: 0.75,     
                                                                overlayClose: false,
                                                                escKey: false } );
                                                }


                                        }
                                
                                 }
                            }      
                        );    


                          /* Add a click handler to the pick data button of datatable */   
                        $(document).on('click', '#'+options['pickButtonId'], 
                            function() {
                                
                                // Ignore action if button disabled
                                if (!$('#'+options['pickButtonId']).attr('disabled')) {

                                    var anSelected = fnGetSelected( oTable );

                                    
                                    if(options['pickCallbackMethod'] && options['pickCallbackMethod']!='')
                                    {
                                        eval(options['pickCallbackMethod']+'(oTable.fnGetData(anSelected[0]));');
                                    }
                                    
                                    
                                    $('#'+options['pickButtonId']).die('click');
                                    $.colorbox.close();

                                }
                            }      
                        );
                            
                            
                            
                            
                        /* Add a click handler to the view button of datatable */   
                        $(document).on('click', '#'+options['viewButtonId'], 
                            function() {
                                // Ignore action if button disabled                               
                                if (!$('#'+options['viewButtonId']).attr('disabled')) {

                                // if there is a call back fn else default colorbox ajax popup
                                if(options['viewButtonFn'] !== null){
                                    options['viewButtonFn'].call();
                                }else{
                                    var anSelected = fnGetSelected( oTable );

                                    if(options['viewAppUrl'] && options['viewAppUrl']!='')
                                        {
                                            //It opens color box popup page. 
                                            $.colorbox( { href:options['viewAppUrl']+passParamsTypeStr+urlencode(oTable.fnGetData(anSelected[0],0)),
                                                        title: 'View record',
                                                        opacity: 0.75,     
                                                        overlayClose: false,
                                                        escKey: false } );
                                        }

                                }
                                }
                            }      
                        );                             




                        if(location.href != top.location.href){ 
                            // the content has been loaded into an IFRAME
                            // control full screen overlay in parent window when colorbox is opened/closed
                            $(document).bind('cbox_open', function() {
                                    
                                    if(options['parentURL']!='' && options['parentURL'])
                                        {
                                            $.postMessage(
                                                'showModalOverlay',
                                                options['parentURL'],
                                                parent
                                            );
                                        }  
                                } );
                            $(document).bind('cbox_closed', function() {
                                    
                                    if(options['parentURL']!='' && options['parentURL'])
                                        {
                                            $.postMessage(
                                                'hideModalOverlay',
                                                options['parentURL'],
                                                parent
                                            );
                                        }
                                        
                                } );
                        }



                        /* Add a click handler to the delete button of colorbox popup form */  
                        $(document).on('click', '#'+options['formDeleteButton'], 
                            function() {

                                if(options['deleteDataUrl']!='' && options['deleteDataUrl'])
                                    {
                                        $.post(options['deleteDataUrl'],        

                                            $("#"+options['colorboxFormId']).serialize(),      
                                            function(data){
                                                // DATA NEXT SENT TO COLORBOX
                                                var p = eval("(" + data + ")");
                                            $.fn.colorbox({
                                                html:   p['message'],
                                                onClosed:function(){ oTable.fnDraw(true); } 
                                                }); 
                                            }); 
                                    }
                                

                            }
                            );


                        /* Add a click handler to the update button of colorbox popup form */  
                    $(document).on('click', '#'+options['formUpdateButton'], 
                            function() {
                                //Validating and posting form data.
                                $('#'+options['colorboxFormId']).validate({
                                            rules: options['frmErrorRules'],
                                            messages: options['frmErrorMessages'],               

                                            errorClass: options['frmErrorMsgClass'],
                                            onkeyup: false,
                                            onblur: false,
                                            errorElement: options['frmErrorElement'],

                                            submitHandler: function() {
                                                
                                                    $("[disabled='disabled']").removeAttr("disabled");
                                                    
                                                    
                                                        if(options['updateDataUrl']!='' && options['updateDataUrl'])
                                                        {
                                                                $.post(options['updateDataUrl'],        

                                                                $("#"+options['colorboxFormId']).serialize(),      
                                                                function(data){
                                                                    // DATA NEXT SENT TO COLORBOX
                                                                var p = eval("(" + data + ")");
                                                                $.fn.colorbox({
                                                                    html:   p['message'],
                                                                    onClosed:function(){ oTable.fnDraw(true); } 
                                                                    }); 
                                                                });
                                                    
                                                        }

                                            }
                                });



                            }
                            );



                     //keyup sugestFieldId for check ID
                    $(document).on('keyup', '#'+options['sugestFieldId'], 
                            function() {

                                    if(options['hiddenPK']!='' && options['hiddenPK']) {
                                        
                                            if(!$('#'+options['hiddenPK']).val()) {

                                                        if(options['sugestFieldId']!='' && options['sugestFieldId'])
                                                        {
                                                                $unique_txt = $('#'+options['sugestFieldId']).val();

                                                                if(options['isExistsDataUrl'] && options['isExistsDataUrl']!='')
                                                                    {
                                                                            $.post(options['isExistsDataUrl'],{pk:$unique_txt},function(result){

                                                                                   if(options['suggestTextId']!='' && options['suggestTextId'])
                                                                                       {
                                                                                            if(result=="null") {        

                                                                                                $('#'+options['suggestTextId']).html(null);
                                                                                                $('#'+options['suggestTextId']).removeClass(options['frmErrorSugMsgClass']);
                                                                                            } 
                                                                                            else 
                                                                                            {
                                                                                                $('#'+options['suggestTextId']).addClass(options['frmErrorSugMsgClass']);
                                                                                                $('#'+options['suggestTextId']).html('The '+options['sugestFieldId']+' you have entered is already exists in our database.');

                                                                                            }
                                                                                       }
                                                                        });
                                                                    }
                                                    }                     
                                        }
                                
                                }

                            });






                        /* Add a click handler to the insert button of colorbox popup form */  

                    $(document).on('click', '#'+options['formInsertButton'], function() {

                        //Validating and posting form data.

                        $('#'+options['colorboxFormId']).validate({
                            rules: options['frmErrorRules'],
                            messages: options['frmErrorMessages'],               

                            errorClass: options['frmErrorMsgClass'],
                            onkeyup: false,
                            onblur: false,
                            errorElement: options['frmErrorElement'],

                            submitHandler: function() {

                                    if(!$('#'+options['suggestTextId']).html()) {
                                        
                                        if(options['createDataUrl']!='' && options['createDataUrl'])
                                            {
                                                $.post(options['createDataUrl'],        

                                                $("#"+options['colorboxFormId']).serialize(),      
                                                function(data){
                                                    // DATA NEXT SENT TO COLORBOX
                                                    var p = eval("(" + data + ")");
                                                $.fn.colorbox({
                                                    html:   p['message'],
                                                    onClosed:function(){ oTable.fnDraw(true); } 
                                                    }); 
                                                });    
                                            }
                                        
                                    }// end if

                                }// end submitHandler
                       }); // validate
                    });



                        /* Add a click handler to the cancel button of colorbox popup form */  
                        $(document).on('click', '#'+options['formCancelButton'], 
                            function() {
                                $.colorbox.close();
                            }      
                        );


                          /* Add a click handler to the view button of colorbox popup form */  
                        $(document).on('click', '#'+options['formViewButton'], 
                            function() {
                                $.colorbox.close();
                            }      
                        );
   



                        jQuery.fn.dataTableExt.oApi.fnFilterOnReturn = function (oSettings) {
                            /*
                            * Usage:       $('#example').dataTable().fnFilterOnReturn();
                            * Author:      Jon Ranes (www.mvccms.com)
                            * License:     GPL v2 or BSD 3 point style
                            * Contact:     jranes /AT\ mvccms.com
                            */
                            var _that = this;

                            this.each(function (i) {
                                $.fn.dataTableExt.iApiIndex = i;
                                var $this = this;
                                var anControl = $('input', _that.fnSettings().aanFeatures.f);
                                anControl.unbind('keyup').bind('keypress', function (e) {
                                    if (e.which == 13) {
                                        $.fn.dataTableExt.iApiIndex = i;
                                        _that.fnFilter(anControl.val());
                                    }
                                });
                                return this;
                            });
                            return this;
                        }            




                        
                        oTable = $('#'+options['htmlTableId']).dataTable( {
                           
                           "aoColumns": options['aoColumns'],
                           //'sDom': 'ft<"dataTables_command">rpli',
                           'sDom': options['sDom'],//'ft<"dataTables_command">rpli',
                            'sPaginationType': 'full_numbers',
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ Records per page",
                                "sSearch": ""+options['searchBoxLabel']+""
                            },
                            'fnDrawCallback': function() {
                               
                               updateHeight(options['htmlTablePageId']);
                                
                               if ( $('#'+options['htmlTableId']+'_paginate span span.paginate_button').size()) 
                               {
                                   
                                    if($('#'+options['htmlTableId']+'_paginate').length > 0){
                                    
                                         $('#'+options['htmlTableId']+'_paginate')[0].style.display = "block";
                                   
                                    }
                                    
                                    if ($('#'+options['htmlTableId']+'_length').length > 0){
                                    
                                         $('#'+options['htmlTableId']+'_length')[0].style.display = "block";
                                   
                                    }
                                    
                                    if ($('#'+options['htmlTableId']+'_info').length > 0){
                                    
                                          $('#'+options['htmlTableId']+'_info')[0].style.display = "block";
                                   
                                    }
                                   
                                   
                               } 
                               else 
                               {
                                   
                                   if($('#'+options['htmlTableId']+'_paginate').length > 0){
                                    
                                         $('#'+options['htmlTableId']+'_paginate')[0].style.display = "none";
                                   
                                    }
                                    
                                    if ($('#'+options['htmlTableId']+'_length').length > 0){
                                    
                                         $('#'+options['htmlTableId']+'_length')[0].style.display = "none";
                                   
                                    }
                                    
                                    if ($('#'+options['htmlTableId']+'_info').length > 0){
                                    
                                          $('#'+options['htmlTableId']+'_info')[0].style.display = "none";
                                   
                                    }
                               }
                               
                               
                                
                                $('.dataTables_command').html(function(){
                                    
                                   var htmlButtons = new Array(); 
                                    
                                    
                                    if(options['addButtonId']!='' && options['addButtonId'])
                                    {
                                        htmlButtons['A'] = '<button id="'+options['addButtonId']+'" type="button" class="gplus-blue"><span class="label">'+options['addButtonText']+'</span></button>';
                                    }
                                    
                                    if(options['updateButtonId'] && options['updateButtonId']!='')
                                    {
                                        htmlButtons['U'] = '<button id="'+options['updateButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">'+options['updateButtonText']+'</span></button>';
                                    }
                                    
                                    if(options['deleteButtonId'] && options['deleteButtonId']!='')
                                    {
                                        htmlButtons['D'] = '<button id="'+ options['deleteButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">'+options['deleteButtonText']+'</span></button>';
                                    }
                                 
                                    if(options['viewButtonId'] && options['viewButtonId']!='')
                                    {
                                        htmlButtons['V'] = '<button id="'+ options['viewButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">'+options['viewButtonText']+'</span></button>';
                                    }
                                        
                                    if(options['pickButtonId'] && options['pickButtonId']!='')
                                    {
                                        htmlButtons['P'] = '<button id="'+ options['pickButtonId']+'" type="button" class="gplus-blue-disabled" disabled="disabled"><span class="label">'+options['pickButtonText']+'</span></button>';
                                    }
                                        
                                        
                                    var html_buttons_str = ''; 
                                    
                                    for (var $hB = 0; $hB < options['displayButtons'].length; $hB++) {
                                    
                                            if(htmlButtons[options['displayButtons'].charAt($hB)])
                                            {
                                                html_buttons_str += htmlButtons[options['displayButtons'].charAt($hB)];
                                            }    
                                    }    
                                      
                                    
                                    return html_buttons_str;
                                });  
                            },
                            'bServerSide': options['bServerSide'],
                            'bProcessing': options['bProcessing'],
                            'bDeferRender': options['bDeferRender'],
                            'bFilter': true,
                            'sAjaxSource': options['fetchDataUrl'],
                            'fnServerData': function ( sSource, aoData, fnCallback ) {
                                                
                                                 if(sSource)
                                                 { 
                                                    //unserialize aoData
                                                    if(options.superFilter != '')
                                                        aoData = $.param(aoData, true) + '&' + $.param(options.superFilter, true);


                                                    $.ajax( { 'dataType': 'json', 
                                                            'type': 'POST', 
                                                            'cache': false,
                                                            'url': sSource, 
                                                            'data': aoData, 
                                                            'success': fnCallback,
                                                            'complete': function() { updateHeight(options['htmlTablePageId']); } 
                                                    } );
                                                }

                                            }

                        } );
                        
                        
                        
                        
                        
                        if(options['searchCloseImage']!='' && options['searchCloseImage'])
                            {
                                $('#'+options['htmlTableId']+'_filter input').wrap('<span></span>').after('<img src="'+options['searchCloseImage']+'" alt="" />');    
                            }


    });

};//$.fn.PCCSDataTable
})( jQuery );
