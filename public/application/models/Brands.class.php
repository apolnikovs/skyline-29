<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Brands Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.02
 *   
 * Changes
 * Date        Version Author                Reason
 * ??/??/????  1.00    Nageswara Rao Kanteti Initial Version
 * 04/07/2012  1.01    Andrew J. Williams    Changes to allow editing of Brand ID
 * 09/01/2013  1.02    Brian Etherington     Moved SKYLINE_BRAND_ID to Constants.class.php
 ******************************************************************************/

class Brands extends CustomModel {
    
    private $conn;
    private $dbColumns  = array('BrandID', 'BrandName', 'Acronym', 'Status');
    private $table = "brand";
    private $tbl;                                                               /* For Table Factory Class */
    static private $brand_id_start = 1000;                                      /* Starting value for BrandID */
    
    //const SKYLINE_BRAND_ID = 1000;

    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );  
        
        $this->tbl = TableFactory::Brand();
        
    }
    
   
    
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->table
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    
    public function fetch($args) {
        
        $NetworkID = isset($args['firstArg'])?$args['firstArg']:'';
        $ClientID  = isset($args['secondArg'])?$args['secondArg']:'';
        
        if($NetworkID!='' || $ClientID!='')
        {
            $sep = '';
            if($NetworkID!='')
            {
                $args['where'] = "NetworkID='".$NetworkID."'";
                $sep = ' AND ';
            }
            
            if($ClientID!='')
            {
                $args['where'] .= $sep."ClientID='".$ClientID."'";
            }
        }
       
        
        $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
       
        return  $output;
        
    }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
     public function processData($args) {
         
         if(!isset($args['BrandID']) || !$args['BrandID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
    
      /**
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param interger $BrandName  
     * @param interger $NetworkID  
     * @param interger $BrandName  
     * @param interger $BrandID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValid($BrandName, $NetworkID, $ClientID, $BrandID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT BrandID FROM '.$this->table.' WHERE BrandName=:BrandName AND NetworkID=:NetworkID AND ClientID=:ClientID AND BrandID!=:BrandID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':BrandName' => $BrandName, ':NetworkID' => $NetworkID, ':ClientID' => $ClientID, ':BrandID' => $BrandID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['BrandID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    
   
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args
       
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     * Changes made to allow the BrandID (primary key) to be set to that of the
     * users choice rather than an auto increment value.
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function create($args) {
        
        
        if(isset($args['AutoSendEmails']) && $args['AutoSendEmails']=="0")
        {
            $args['AutoSendEmails'] = 0;
        }   
        else
        {
            $args['AutoSendEmails'] = 1;
        }
        
        
        
        
//        if(isset($args['SendRepairCompleteTextMessage']) && $args['SendRepairCompleteTextMessage']=="Yes")
//        {
//            $args['SendRepairCompleteTextMessage'] = 'Yes';
//        }   
//        else
//        {
//            $args['SendRepairCompleteTextMessage'] = 'No';
//        }
//        
        
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (BrandID, BrandName, Acronym, NetworkID, ClientID, BrandLogo, AutoSendEmails, EmailType, TrackerURL, Status, CreatedDate)
                                      VALUES(:BrandID, :BrandName, :Acronym, :NetworkID, :ClientID, :BrandLogo, :AutoSendEmails, :EmailType, :TrackerURL, :Status, :CreatedDate)';
        
       
        
        if($this->isValid($args['BrandName'], $args['NetworkID'], $args['ClientID'], 0))
        {
            
           
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
           
            $result =  $insertQuery->execute(array(
                ':BrandID' => $args['NewBrandID'],
                ':BrandName' => $args['BrandName'], 
                ':Acronym' => $args['Acronym'], 
                ':NetworkID' => $args['NetworkID'], 
                ':ClientID' => $args['ClientID'],
                ':BrandLogo' => '',
                ':AutoSendEmails' => $args['AutoSendEmails'],
               // ':SendRepairCompleteTextMessage' => $args['SendRepairCompleteTextMessage'],
               // ':SMSID' => $args['SMSID'],
                ':EmailType' => $args['EmailType'],  
                ':TrackerURL' => $args['TrackerURL'],
                ':Status' => $args['Status'],
                ':CreatedDate' => date("Y-m-d H:i:s")
                ));
        
            
               $BrandID = $args['NewBrandID'];
            
               $result2 = false; 
               if($result)//if the brand details are inserted into database then only updaing logo name with name brand id
               {
                     $newBrandLogo = $this->imageUpload($args['UploadedBrandLogo'], $BrandID);
                     
                     
                     
                     $update_sql = 'UPDATE '.$this->table.' SET BrandLogo=:BrandLogo WHERE BrandID=:BrandID';
                     
                     $updateQuery = $this->conn->prepare($update_sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                     $result2 = $updateQuery->execute(
                      
                      array(
                          
                            ':BrandLogo' => $newBrandLogo, 
                            ':BrandID' => $BrandID
                          
                          )
                      
                      );
               }
               
              if($result && $result2)
              {
                    return array('status' => 'OK',
                            'message' => $this->controller->page['Text']['data_inserted_msg']);
              }
              else
              {
                  return array('status' => 'ERROR',
                            'message' => $this->controller->page['Errors']['data_not_processed']);
              }
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    /**
     * Description
     * 
     * This method is used for to rename the uploaded image/ if the image is not uploaded then it copies the default image.
     *
     * @param string $argUploadedBrandLogo
     * @param string $BrandID
     * @param string $oldImgName  Default null
     * @return string $newBrandLogo It contains new image name.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function imageUpload($argUploadedBrandLogo, $BrandID, $oldImgName=null) {
        
        
        $ImgExtentions     = array('gif','png','jpg','jpeg','bmp');
        $imageNotUploaded  = true;
        $brandLogosDirPath =  APPLICATION_PATH."/../".$this->controller->config['Path']['brandLogosPath'];

        if($argUploadedBrandLogo)
        {    

            foreach($ImgExtentions as $eKey=>$eValue)
            {

                $UploadedBrandLogo =  $argUploadedBrandLogo.".".$eValue;

                $fileExists = file_exists ( $brandLogosDirPath.$UploadedBrandLogo );

                if($fileExists)
                {
                    //Deleting old image as we are uploading new image.
                    if($oldImgName)
                    {    
                        @unlink($brandLogosDirPath.$oldImgName);
                    }
                    
                    $newBrandLogo =  $BrandID.'_logo.'.$eValue;
                    @rename ( $brandLogosDirPath.$UploadedBrandLogo , $brandLogosDirPath.$newBrandLogo);
                    $imageNotUploaded = false;
                    break;
                }    

            } 
        }

        //If user doesn't upload image then we are assigning default brand image.
        if($imageNotUploaded)
        {
            if($oldImgName)//This is the case when image image is not selected in the update page.
            {
                $ext = substr( $oldImgName, ( strrpos($oldImgName,'.')+1 ) );
                $newBrandLogo = $BrandID.'_logo.'.$ext;
                
                @rename($brandLogosDirPath.$oldImgName, $brandLogosDirPath.$newBrandLogo);
            }
            else
            {
//                $UploadedBrandLogo   = "default_logo.png";
//                $newBrandLogo =  $BrandID.'_logo.png';
//                @copy ( $brandLogosDirPath.$UploadedBrandLogo , $brandLogosDirPath.$newBrandLogo);
                
                 $newBrandLogo = NULL;
            }
            

        }
        
        
        return $newBrandLogo;
    }
    

    
    
    
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function fetchRow($args) {

        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT BrandID, BrandName, NetworkID, ClientID, BrandLogo, AutoSendEmails, EmailType, TrackerURL, Status, Acronym FROM '.$this->table.' WHERE BrandID=:BrandID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':BrandID' => $args['BrandID']));
        $result = $fetchQuery->fetch();
        
        
        return $result;
    }
    
    
    /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     * Changes made to allow the BrandID (primary key) to be set to that of the
     * users choice rather than an auto increment value.
     * @author Andrew Williams <a.williams@pccsuk.com>
     * 
     **************************************************************************/ 
    public function update($args) {
        
        if($this->isValid($args['BrandName'], $args['NetworkID'], $args['ClientID'], $args['BrandID']))
        {        
            
            if(isset($args['AutoSendEmails']) && $args['AutoSendEmails']=="0")
            {
                $args['AutoSendEmails'] = 0;
            }   
            else
            {
                $args['AutoSendEmails'] = 1;
            }
            
            
//            if(isset($args['SendRepairCompleteTextMessage']) && $args['SendRepairCompleteTextMessage']=="Yes")
//            {
//                $args['SendRepairCompleteTextMessage'] = 'Yes';
//            }   
//            else
//            {
//                $args['SendRepairCompleteTextMessage'] = 'No';
//            }
            
            
            //Uploading image...
            $newBrandLogo = $this->imageUpload($args['UploadedBrandLogo'], $args['NewBrandID'], $args['hiddenBrandLogo']);
                 
            
            /* Execute a prepared statement by passing an array of values */
            $sql = 'UPDATE '.$this->table.' SET BrandID=:NewBrandID, BrandName=:BrandName, Acronym=:Acronym, NetworkID=:NetworkID, ClientID=:ClientID, BrandLogo=:BrandLogo, AutoSendEmails=:AutoSendEmails, EmailType=:EmailType, TrackerURL=:TrackerURL, Status=:Status WHERE BrandID=:BrandID';
        
            
          
            
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $result = $updateQuery->execute(
                      
                      array(
                          
                            ':NewBrandID' => $args['NewBrandID'],
                            ':BrandName' => $args['BrandName'], 
                            ':Acronym' => $args['Acronym'], 
                            ':NetworkID' => $args['NetworkID'], 
                            ':ClientID' => $args['ClientID'],
                            ':BrandLogo' => $newBrandLogo,
                            ':AutoSendEmails' => $args['AutoSendEmails'],
//                            ':SendRepairCompleteTextMessage' => $args['SendRepairCompleteTextMessage'],
//                            ':SMSID' => $args['SMSID'],
                            ':EmailType' => $args['EmailType'], 
                            ':TrackerURL' => $args['TrackerURL'],
                            ':Status' => $args['Status'],
                            ':BrandID' => $args['BrandID']
                          
                          )
                      
                      );
        
               
               
                if($result)
                {
                        return array('status' => 'OK',
                                'message' => $this->controller->page['Text']['data_updated_msg']);
                }
                else
                {
                    return array('status' => 'ERROR',
                                'message' => $this->controller->page['Errors']['data_not_processed']);
                }
              
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['data_deleted_msg']);
    }
    
    /**
     * getNextBrandID
     *  
     * Create a customer
     * 
     * @return integer  Suggest ID of next Brand ID
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getNextBrandID() {
       
        $sql = "
                SELECT
			BrandID
		FROM
			brand
		ORDER BY
			BrandID DESC
		LIMIT
			1
               ";
                     
        $result = $this->Query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            $id = $result[0]['BrandID'] + 1;
        } else {
            $id = $this->brand_id_start;
        }
        
        return($id);
    }
     
    /**
     * brandExists
     *  
     * Return true if brand exists, false otherwise
     * 
     * @param integer $bID  Brand ID
     * 
     * @return boolean  True
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function brandExists($bID) {
       
        $sql = "
                SELECT
			BrandID
		FROM
			brand
		WHERE
			BrandID = $bID
               ";
                     
        $result = $this->query($this->conn, $sql);
        
        if ( count($result) > 0 ) {
            return(true);
        } else {
            return(false);
        }
    }
    

    
    public function getAllBrands() {
	
	$q = "SELECT * FROM brand";
        $result = $this->query($this->conn, $q);
	return $result;
	
    }

    
    
    public function getNetworkBrands($networkID) {
	
	$q = "SELECT * FROM brand WHERE NetworkID = :networkID";
        $result = $this->query($this->conn, $q, array("networkID" => $networkID));
	foreach($result as $el) {
	    $return[$el["BrandID"]] = $el["BrandName"];
	}
	return $return;
	
    }

    
    
    public function getClientBrands($clientID) {
	
	$q = "SELECT * FROM brand WHERE ClientID = :clientID";
        $result = $this->query($this->conn, $q, array("clientID" => $clientID));
	foreach($result as $el) {
	    $return[$el["BrandID"]] = $el["BrandName"];
	}
	return $return;
	
    }
    
    
    
    public function getBrandByID($brandID) {
	
	$q = "SELECT * FROM brand WHERE BrandID = :brandID";
	$values = ["brandID" => $brandID];
	$result = $this->query($this->conn, $q, $values);
	return (@$result[0]) ? @$result[0] : false;
	
    }
   
    
    
    public function getBrandByBranchID($branchID) {
	
	$q = "	SELECT	    * 
		FROM	    branch
		LEFT JOIN   brand_branch ON branch.BranchID = brand_branch.BranchID
		WHERE	    branch.BranchID = :branchID
	     ";
	
	$values = ["branchID" => $branchID];
	$result = $this->query($this->conn, $q, $values);
	
	return (isset($result[0])) ? $result[0] : false;
	
    }
    
    public function getBrandLogo( $brand ) {
        
        if (is_int($brand)) {
            $sql = "select BrandLogo from brand where BrandID=:Brand";
        } else {
            $sql = "select BrandLogo from brand where BrandName=:Brand";
        }
        
        $params = array( 'Brand' => $brand );
        $result = $this->Query($this->conn, $sql, $params);
        
        if (count($result) == 0)
            return '1000_logo.png';
        
        return $result[0]['BrandLogo'];
    }
    
    
    
}
?>
