<?php

/**
 * 
 * Washi MVC Front Controller.
 * 
 * Parse the URl and route requests to the appropriate Controller class 
 * and Action method.
 * 
 * The routing scheme is http://domain[/sub_folder]/[Application$]Controller/Action/Param 1/Param 2/.../Param n[/?Get Paramaters]
 * 
 * If present sub_domain will be assigned to SUB_DOMAIN. This makes it easy to test
 * applications without setting up Virtual Hosts.
 * 
 * If present Application will be used as a subfolder location for the 
 * controller class include.  This makes it easy to store controllers in 
 * separate application groups in a large web site application.
 * 
 * If omitted or is 'index' the controller class will be DefaultController in
 * file /application/controllers/DefaultController.class.php
 * 
 * If omitted the action method will be indexAction.
 * 
 * The list of parametrs may either be positional e.g. /value 0/value 1/value 3...
 * or of the form name=value eg /p1=value/p2=value/p3=value
 * 
 * The parameters will be passed to the Action method as either 
 * an indexed array ( 0=>value 0, 1 => value 1 ....)
 * or an Associative array ( p1 => value 1, p2 => value 2....)
 *  
 * example: public function indexAction( $args ) { .... }
 * 
 * @author     Brian Etherington <brian@smokingun.co.uk>
 * @copyright  2011 Smokingun Graphics
 * @link       http://www.smokingun.co.uk
 * @version    1.6
 * 
 *  * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Brian Etherington     Initial Version 
 * 01/06/2012  1.2     Brian Etherington     Added Smarty 3.1 compatability
 * 11/11/2012  1.3     Brian Etherington     Added helpers folder to include path
 * 22/12/2012  1.4     Andrew Williams       Don't display generated page timne in SOAP call
 * 30/12/2012  1.5     Vykintas Rutkunas     Added 3rd party library - mPDF
 * 31/01/2013  1.6     Brian Etherington     Stop parsing arguments when GET parameters are found
 *****************************************************************************/

$execStart = explode(" ", microtime())[1] + explode(" ", microtime())[0];

// Define path to application directory
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

// Set Timezone....
date_default_timezone_set('Europe/London');

// Define Global variable to indicate a Cron Job
$GLOBALS['IS_CRON_JOB'] = false;

// Define Global variable to indicate ImageServer controller
$GLOBALS['IS_IMAGE_SERVER'] = false;

class ErrorOrWarningException extends Exception {
    
    protected $_Context = null;
    
    public function getContext() {
        return $this->_Context;
    }
    
    public function setContext( $value ) {
        $this->_Context = $value;
    }

    public function __construct( $code, $message, $file, $line, $context ) {
        parent::__construct( $message, $code );

        $this->file = $file;
        $this->line = $line;
        $this->setContext( $context );
    }
}

function errorHandler( $code, $message, $file, $line, $context ) {
    // Respect error_reporting setting.
    // This is required for SMARTY 3.10
    if (!(error_reporting() & $code)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorOrWarningException( $code, $message, $file, $line, $context );
}

/*
 * Instantiate an instance of the appropriate controller class 
 * and dispatch the request to the requested Action Method.
 * 
 * @param  string $controller 
 * @param  string $method
 * @param  array  args
 */
function dispatch($controller, $method, $args) {
           
    // separate controller name from controller path
    $pathelt = explode('/',$controller);
    $numelts = count($pathelt);
    $controllerClass = $numelts == 1 ? $controller : $pathelt[$numelts-1];
      
    $file = APPLICATION_PATH . '/controllers/' . $controller . '.class.php';
    include_once($file);
    unset($file);
        
    // Prevent uncatchable PHP fatal Error if Class does not exist....
    if (!class_exists($controllerClass, FALSE)) {
       throw new Exception($controllerClass . ' not found.');
    }
       
    // create controller instance and call the specified method
    $cont = new $controllerClass;
    //if (!method_exists($cont, $method)) {
    // 1.1 use is_callable to make sure method is public and
    // to allow for __call magic method in RESTful controllers
    if (!is_callable(array($cont, $method))) {
        throw new Exception($controllerClass . ' -> ' . $method . ' not found.');
    }
    $cont->$method($args);
}

/****************************************************************************
 * 
 *  Application Front Controller
 *  ============================
 * 
 *  handle request and dispatch it to the appropriate controller/method
 * 
 ****************************************************************************/

try {
    
    // Note: set_error_handler does not catch PHP fatal errors!!
    set_error_handler( 'errorHandler' );
    
    // set include path
    $path = APPLICATION_PATH . '/include' . PATH_SEPARATOR . APPLICATION_PATH . '/helpers' ;
    set_include_path(get_include_path() . PATH_SEPARATOR . $path);
    
    $url = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
       
    // check for sub-domain...
    $docpath = dirname($_SERVER['SCRIPT_FILENAME']);
    if (strlen($docpath) > strlen($_SERVER['DOCUMENT_ROOT'])) {
    	$subdomain = substr($docpath, strlen($_SERVER['DOCUMENT_ROOT']));
    	define('SUB_DOMAIN', $subdomain);
    	$mypath = explode('/', trim($subdomain, '/'));
    	foreach ($mypath as $s) {
    		array_shift($url);
    	}
    }
   
    // force default controller if index.php or index...
    if ( count($url) >  0 
         && ((strpos($url[0],'.') !== FALSE) || ($url[0] == 'index')) ) {
        $url[0] = null;
    } 
    
    // get controller name
    $controller = empty($url[0]) ?  'DefaultController' : str_replace('$','/',$url[0]) . 'Controller';
    array_shift($url);
    
    // strip any get parameters from method name
    $pos = empty($url[0]) ? false : strpos($url[0], '?');
    if ($pos !== false) $url[0] = substr( $url[0], 0,$pos );
    
    // get method name
    $method = empty($url[0]) ? 'indexAction' : $url[0] . 'Action';
    array_shift($url);
    
    // get arguments passed in to the method
    $args = array();
    foreach($url as $item) {
        // stop parsing if arguement begins with ?
        // and assume that this is the start of the GET parameters.
        if (substr($item,0,1) == '?') break;
        // check if the argument is associative (name = value) or positional
        $elt = explode('=',urldecode($item));
        if (count($elt) == 1) {
            $args[] = $elt[0];
        } else {     
            $args[$elt[0]] = $elt[1];
        }
    }
    
    //throw new Exception('Debugging...');
          
    dispatch($controller, $method, $args); 
    
    //Check if it's not AJAX call and display page generation time.
    // Also check that a SOAP call is not used (assumption any SOAP controller will have the text 'Soap' in the URL
    if((!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH']!=='XMLHttpRequest') 
            && !stristr($_SERVER["REQUEST_URI"], "/api/") 
            && !stristr($_SERVER["REQUEST_URI"], "/jobsTableExport/") 
            && !stristr($_SERVER["REQUEST_URI"], "/RemoteEngineerHistory/") 
            && !stristr($_SERVER["REQUEST_URI"], "StatusChangeReport") 
            && !stristr(strtolower($_SERVER["REQUEST_URI"]), "soap") 
            && !stristr($_SERVER["REQUEST_URI"], "printJobRecord")
            && !$GLOBALS['IS_CRON_JOB']
            && !$GLOBALS['IS_IMAGE_SERVER']) {
	$execEnd = explode(" ", microtime())[1] + explode(" ", microtime())[0];
	$execTotal = ($execEnd - $execStart);
//	echo('<div style="width:950px;margin:auto;">
//            <p class="PageGenerationMessage" style="text-align:left;float:left">Page generation ' . round($execTotal, 2) . ' seconds</p>
//             <p class="PageGenerationMessage" style="text-align:right;float:right">Copyright &#169; '.date('Y',strtotime('now')).' PC Control Systems Ltd. All rights reserved. Powered by <a target="_blank" href="http://pccsuk.com/">pccsuk.com</a></p>   
//            </div></body></html>');
	echo('<div style="margin:auto;width:100%;">
         
             <p class="PageGenerationMessage" style="text-align:center;">Copyright &#169; '.date('Y',strtotime('now')).' PC Control Systems Ltd. All rights reserved. Page generated by <a target="_blank" href="http://pccsuk.com/">pccsuk.com</a> in ' . round($execTotal, 2) . ' seconds</p>   
            </div></body></html>');
    }
    
    
} catch (Exception $e){
         
   dispatch('ErrorController','indexAction', 
            array( 'Controller' => $controller,
                   'method' => $method,
                   'args' => implode('/',$url),
                   'Error' => $e->getMessage(),
                   'File' => $e->getFile(),
                   'Line' => $e->getLine(),
                   'Trace' => $e->getTraceAsString()));
    
}
// End front controller
