<head>
</head>
<body> 
<div id="popUpContainer" style="width:926px">
             <div id="appDetails" >
         <fieldset>
             <legend>Data Exports</legend>
              <form method=post action="{$_subdomain}/Diary/exportDiaryData" id="exportForm">
                 
                  <input type="hidden" name="type" id="exportType" value="">
                 
             <table valign=top style="vertical-align:top !important">
                
                 <tr>
                     <td style="vertical-align:top !important;" >
                         <fieldset style="height:300px;padding-top: 10px;width:330px;overflow-y: auto">
                        <table id="export_table" border="0" cellpadding="0" cellspacing="0" class="browse" >
                 
                             <thead>
                     <tr>
                <th class="diaryDataTable" style="background-position:right bottom;">id</th>
                <th class="diaryDataTable" style="background-position:right bottom;">Select Required Export</th>
                </tr>
                </thead>
                <tbody >
                    {if $diaryType=="FullViamente"}
           <tr>
                    
                            <td class="diaryDataTable" >1</td>
                            <td class="diaryDataTable" >Engineer Time Summary</td>
                           
                      
                    </tr>
                     
                    <tr>
                            <td class="diaryDataTable" >2</td>
                            <td class="diaryDataTable" >APPOINTMENT DETAILS & OPTIMISATION RESULTS</td>
                      
                    </tr>
                    {elseif $diaryType=="NoViamente"}
                         <tr>
                            <td class="diaryDataTable" >3</td>
                            <td class="diaryDataTable" >APPOINTMENT TABLE EXPORT</td>
                      
                    </tr>
                    {else}
                        <tr>
                            <td class="diaryDataTable" >4</td>
                            <td class="diaryDataTable" >APPOINTMENT TABLE EXPORT</td>
                      
                    </tr>
                        {/if}
          
              </tbody>
             </table>
                     
                     </fieldset>
                     </td>
                     <td>
                         <fieldset style="width:330px;height:300px;padding-top: 10px;display:block" >
                             <div style="margin-bottom: 30px;position:relative;float:right;width:330px;text-align: right" >
                                 <input checked=checked id="tagAllEngineersExport" onclick="$('#fl2').find(':checkbox').attr('checked', 'checked');$('#tagAllEngineersExport2').attr('checked',false)" type="checkbox">Tag All &nbsp;&nbsp;&nbsp;
                                 <input  id="tagAllEngineersExport2" onclick="$('#fl2').find(':checkbox').attr('checked', false);$('#tagAllEngineersExport').attr('checked',false)" type="checkbox">Untag All 
                             </div>
                             
                             <fieldset style="padding-top: 10px;height:231px;overflow:auto;" id="fl2">
                                 <div style="height:219px;overflow:auto;">
                                    
                           {foreach $engineerList as $ff}
                              <input    type="checkbox" checked=checked value="{$ff.ServiceProviderEngineerID}" name="engIDExport[]">{if isset($ff.EngineerFirstName)}{$ff.EngineerFirstName}{/if}{if isset($ff.EngineerLastName)} {$ff.EngineerLastName}{/if}<br>
                           {/foreach}
                                 </div>
                         </fieldset>
                         </fieldset>
                     
                     </td>
                         
                    
                 </tr>
                  <tr>
                      <td colspan="10" style="background: none">Date From<span style=color:red>*</span>
                     
                   
                        <input type="text" name="dateFrom"  style="width:80px;" id="dateFrom" value="" readonly="readonly">
<!--                        <img class="ui-datepicker-trigger" src="/css/Base/images/calendar.png" alt="..." title="...">-->
                 
                   
                    
                         Date To<span style=color:red>*</span>
                     
                     
                 <input type="text" name="dateTo"  style="width:80px;" id="dateTo" value="" readonly="readonly">
<!--                    <img class="ui-datepicker-trigger" src="/css/Base/images/calendar.png" alt="..." title="...">-->
                   
                     
                  
               
                  
                 </tr>
                 <tr>
                     <td colspan=10>
<!--                         <button type="button" class="btnStandard" style="color:white" onclick="addToDate('m')">MTD</button> 
                         <button type="button" class="btnStandard" style="color:white" onclick="addToDate('30')">30 Days</button> 
                         <button type="button" class="btnStandard" style="color:white" onclick="addToDate('60')">60 Days</button> 
                         <button type="button" class="btnStandard" style="color:white" onclick="addToDate('90')">90 Days</button> 
                         <button type="button" class="btnStandard" style="color:white" onclick="addToDate('y')">YTD</button> -->
                     </td>
                 </tr>
                 </table

                          
 </form>
 <img onclick="submitExport('exportForm')" Title="Export Data" style="float:right;cursor:pointer" src="{$_subdomain}/css/Skins/{$_theme}/images/icons2_export_routes.png">
   
    </div>
 
          
              <script>
                  function parseDate(input) {
  var parts = input.split('/');
  // new Date(year, month [, date [, hours[, minutes[, seconds[, ms]]]]])
  return new Date(parts[2], parts[1]-1, parts[0]); // months are 0-based
}
                  function submitExport(f){
                 
        if($('#dateFrom').val()!="" && $('#dateTo').val()!=""&&$('input[name="engIDExport[]"]:checked').length > 0){
        
        if(parseDate($('#dateFrom').val())<=parseDate($('#dateTo').val())){
        if( $('#exportType').val()!=""){
   $('#'+f).submit();
   }else{
   alert('Please select export type first');
   }}else{
   alert('Date range is set wrong please correct error and try again!');
   }
}else{
alert('All fields should be filled, please fill all fields and try again');
}
          }       
                  
        $(document).ready(function() {
      
  });//end document ready

function addToDate(type)
{
    if($('#dateFrom').val()!=""){
        var dateA=$('#dateFrom').val().split('/');
        var date = new Date($('#dateFrom').val());
     d = date.getDate();
      m = date.getMonth();
      y = date.getFullYear();
    
if(type=="m"){ var edate= new Date(y, m+1, d);}
if(type=="30"){ var edate= new Date(y, m, d+30);}
if(type=="60"){ var edate= new Date(y, m, d+60);}
if(type=="90"){ var edate= new Date(y, m, d+3);}

if(type!='y'){
 var d1 =(edate.getDate());
     var m1 = (edate.getMonth()+1);
     var y1 = edate.getFullYear();
     }
     
     if(type=="y"){ d1=d;m1=m;y1=y;}
     
     
     if(d1<10){ d1="0"+d1}
     if(m1<10){ m1="0"+m1}
$('#dateTo').val(d1+'/'+m1+'/'+y1);
}
}
    </script>  
                      </body>
<!--        end job  details field set    -->
     
 
         