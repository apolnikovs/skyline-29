<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Skyline Diary Printout</title>
<style type="text/css" >
body{
  font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
}
#diary_manager{
	position:relative;float:left;zoom:1;margin:0px;padding-top:3px; width:770px; line-height:13px;
	font-size: 11px;
}

.logo{
	float:left;position:absolute;top:0px;left:0px;
}

.red{
  color:red;
}

.black{
  color:black;
}

.blue{
  color:#336699;
}

body#diary_manager table.blue td{
	color: #979797;
	font-family:"Lucida Sans Unicode", "Lucida Grande", sans-serif;
	font-weight: normal;
	margin-top: 0;
	margin-bottom: 4px;
	text-align: left;
	text-transform:normal;
}

body#diary_manager table.blue td.gray{
	color:#000000;
  padding-left:10px;
  padding-right: 5px;
}

body#diary_manager table.blue td.bold{
	font-weight:bold;
}

body#diary_manager table.blue td.normal{
	text-transform:capitalize;
       font-size:5px;
}

@media all {
  .page-break  { display: none; }
}

@media print {
  .page-break  { display: block; page-break-before: always; }
}

.borders{
    border-left:1px dashed #eee;border-right:1px dashed #eee;border-bottom:1px dashed #eee;
    }
    .bot{
    border-bottom:1px dashed #eee;
    padding-bottom:15px;
    }
    .top{
        padding-top:15px;
        }
    .normal{
        position:fixed;
        table-layout:fixed;
word-wrap:break-word;
        font-size:10px;
        }
</style>
</head>

<body class="main" id="diary_manager">
    {if $data['engineers']|@count==0}
        <div style="text-align:center !important;margin:auto !important"> <h2 style="text-align:center !important;line-height: 2">No Appointments Available To Print For Selected Date and Engineer(s). Please select alternate date or Engineers.</h2></div>
    {/if}
     {for $er=0 to $data['engineers']|@count-1}
  
     
     
<div style="page-break-inside: avoid;">
     
<table class="blue" class="normal" width="100%" cellspacing="4" cellpadding="0" style="padding-bottom:2px;border-bottom:1px solid #dadada;">
  <tr>
    <td width="50%" rowspan="6"><img src="{$_subdomain}/images/brandLogos/{$_brandLogo|default: 'default_logo.png'}" alt="" id="logo" border="0"/></td>
    <td width="17%" style="font-size:10px;">Date</td>
    <td width="28%" class="normal" style="font-size:13px;font-weight:bolder;padding-left:10px;color:black;">{$data['engineers'][$er].date|date_format: "%A %e %B %Y"}</td>
  </tr>
  <tr>
    <td style="font-size:10px;">Driver</td>
    <td class="normal" style="font-size:13px;font-weight:bolder;padding-left:10px;color:black;">{$data['engineers'][$er].name}</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
        {if $diaryType=="FullViamente"}
  <tr>
    <td style="font-size:10px;">Start</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].startTime} - {$data['engineers'][$er].startLocation} ({$data['engineers'][$er].startPlace})</td>
  </tr>
  <tr>
    <td style="font-size:10px;">Finish</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].endTime} - {$data['engineers'][$er].endLocation} ({$data['engineers'][$er].endPlace})</td>
  </tr>
  <tr>
    <td style="font-size:10px;">Service Interval</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].serviceInterval|date_format:"%H:%M"} ({$data['engineers'][$er].siP}%)</td>
  </tr>
  <tr>
    <td rowspan="3" style="text-align:left; padding-left:30px;padding-bottom:20px; font-weight:bold; text-transform:uppercase;color:#336699;font-size:15px;">Diary Manager</td>
    <td style="font-size:10px;">Travel Time</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].travelTime} ({$data['engineers'][$er].tiP}%)</td>
  </tr>
  <tr>
    <td style="font-size:10px;">Total Hours</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].totalHours}</td>
  </tr>
  {/IF}
  <tr>
    <td style="font-size:10px;">Appointments</td>
    <td class="normal bold" style="padding-left:10px;">{$data['engineers'][$er].totalAppointments}      {if $diaryType=="FullViamente"} Average Time: {$data['engineers'][$er].averageAp}{/if}</td>
  </tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0" style="padding-bottom:2px;border-bottom:1px solid #dadada;">
  <tr>
  <td class="normal" rowspan="10" width="6%" class="bold">
    <img style="padding-left:10px;" src="{$_subdomain}/css/Base/images/truck_in.jpg" /></td>
    <td width="10%">{$data['engineers'][$er].startPlace}</td>
    <td  width="10%">{$data['engineers'][$er].startLocation} </td>
    <td width="50%" rowspan="6" valign="middle" align="center" class="gray normal" style="text-align:center;font-size:10px;text-transform:uppercase;font-weight:bold;">      {if $diaryType=="FullViamente"}START{else} NOTES {/if}</td>
        {if $diaryType=="FullViamente"}
    <td width="15%">Start Time: {$data['engineers'][$er].startTime|date_format:"%H:%M"}</td>
    {/if}
</tr>
</table>

{$eID=$data['engineers'][$er].ServiceProviderEngineerID}
    {foreach from=$data['app'].$eID key=k item=v name=foo}
        
<table class="blue" border=0 cellpadding="10px" cellspacing="0px" width="100%" cellspacing="1" cellpadding="1" style="padding: 10px; padding-bottom:2px;border:0px dashed #eee;border-collapse:collapse">

  <tr class="top">
    <td rowspan="10" width="6%" class="bold">
        <img style="" src="{$_subdomain}/css/Base/images/map_pin.jpg" />
    <div style=""> &nbsp;&nbsp;&nbsp;{$smarty.foreach.foo.index+1}</div>
   </td>
    <td  class="gray normal top">Customer:</td>
    <td class="gray normal top">{$v.name}</td>
    
    <td width="" rowspan="10" valign="middle" align="center" class="gray normal borders" >{$v.Notes|cat:" "}{$v.PreVisitContactInfo}</td>
          {if $diaryType=="FullViamente"}
    <td align="right" width="12%" class="gray normal">Travel Time:</td>
    <td width="12%" class="gray normal" align="right" style="color:red;font-weight:bold;">{$v.travelTime|date_format:"%H:%M"}</td>
    {/if}
  </tr>
  <tr>
   <td class="gray normal">Phone No:</td>
    <td class="gray normal">{$v.phone}</td>
    
  </tr>
  <tr >
     
    <td class="gray normal">Location:</td>
    <td class="gray normal">{$v.location}</td>
          {if $diaryType=="FullViamente"}
    <td rowspan="2" align="right" class="gray normal" >Arrive:</td>
    <td rowspan="2" align="right" class="gray normal" style="font-weight:bold;">{$v.ViamenteStartTime|date_format:"%H:%M"}</td>
    {/if}
  </tr>
  
  <tr>
      <td  class="gray normal">Job No:</td>
    <td  class="gray normal">{$v.jobNo}</td>
  </tr>
  <tr>
   <td nowrap=nowrap width="10%" class="gray normal" >Site:</td>
    <td heigth="0px"  nowrap=nowrap width="20%" class="gray normal" >{$v.Postcode}</td>
          {if $diaryType=="FullViamente"}
    <td rowspan="2" align="right" class="gray normal">Service Interval:</td>
    <td rowspan="2" class="gray normal" align="right" style="color:#239ff6;font-weight:bold;">{$v.Duration}</td>
    {/if}
  </tr>
  <tr>
      
       <td class="gray normal">Time Window:</td>
    <td class="gray normal">{$v.AppointmentTime} ({$v.AppointmentStartTime|date_format:"%H:%M"}-{$v.AppointmentEndTime|date_format:"%H:%M"}) 
    {if $v.AppointmentStartTime2!=""}({$v.AppointmentStartTime2|date_format:"%H:%M"}-{$v.AppointmentEndTime2|date_format:"%H:%M"}) {/if}
    </td>
  </tr>
  <tr>
    <td class="gray normal">App Type:</td>
    <td class="gray normal">{$v.AppointmentType}</td>
  </tr>
  <tr>
    <td class="gray normal bot">Product:</td>
    <td class="gray normal bot" >{$v.ProductType}</td>
          {if $diaryType=="FullViamente"}
    <td class="gray bot normal" rowspan="2" align="right">Depart:</td>
    <td rowspan="2" align="right" class="gray bot normal" style="font-weight:bold">{$v.DepartTime|date_format:"%H:%M"}</td>
    {/if}
  </tr>
  
</table>
        
 </div>       
        {/foreach}

        <table width="100%" cellspacing="0" cellpadding="0" style="padding-bottom:2px;">
  <tr>
  <td rowspan="10" width="6%" class="bold">
    <img style="padding-left:10px;" src="{$_subdomain}/css/Base/images/truck_out.jpg" />
  </td>
    <td width="10%">{$data['engineers'][$er].endPlace}</td>
    <td width="13%" class="gray" style="padding-left:10px;">{$data['engineers'][$er].endLocation}</td>
          {if $diaryType=="FullViamente"}
    <td rowspan="10" valign="middle" align="center" class="gray normal" style="text-align:center;width:370px;font-size:10px;text-transform:uppercase;font-weight:bold;">FINISH</td>
    {/if}
<!--    <td width="14%">Travel Time:</td>
    <td class="gray" style="color:red;font-weight:bold;">{$v.travelTime|date_format:"%H:%M"}</td>-->
      {if $diaryType=="FullViamente"}
<td width="15%"><span style="margin-left:36px;margin-right:66px">Arrive:</span></td>
<td class="gray" style="color:gray;font-weight:bold;font-size:12px" align="left">{$data['engineers'][$er].endTime|date_format:"%H:%M"}</td>
{/if}
</tr>
<tr>
<td colspan="1"></td>

</tr>
</table>
{if $er!=$data['engineers']|@count-1}
     <p class="page-break">    </p>
     {/if}
{/for}


</body>
</html>
