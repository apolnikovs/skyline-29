# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.188');

# ---------------------------------------------------------------------- #
# Add table "branch"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `branch` ADD `CurrentLocationOfProduct` ENUM( 'Customer', 'Branch', 'Service Provider', 'Supplier', 'other' ) NULL DEFAULT NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.189');
