<div id="menu">
    
    {if $session_user_id == ''}
	
	{menu menu_items = [
	    [$HelpPage,'Help/Legal','/index/help',''],
	    [$HomePage,'Login','/Login','']
	]}
	
    {else}
			
	{*if $superadmin*}
	{if $loggedin_user->Username == "sa"}
	    
	    {menu menu_items = [
		[$RAJobsPage, 'RA&nbsp;Jobs','/Job/raJobs','first'],
		[$OutstandingJobsPage, 'Open&nbsp;Jobs','/Job/openJobs',''],
		[$OverdueJobsPage,'Overdue&nbsp;Jobs','/Job/overdueJobs',''],
		[$ClosedJobsPage,'Closed&nbsp;Jobs','/Job/closedJobs',''],
		[$GraphicalAnalysisPage,'Graphical&nbsp;Analysis','/Performance/JobsGauges/OpenTAT',''],
		[$PerformancePage,'Performance','/Performance/JobsGauges/OpenJobs',''],
		[$LinksPage,'Site Map','/index/siteMap',''],
		[$HelpPage,'Help/Legal','/index/help',''],
		[$DiaryInterface,'Diary Interface','/Diary/default','']
	    ]}
	    
	{else}
	    
	    {menu menu_items = $menuItems}
	    
	{/if}
			
    {/if}
    
</div> 
